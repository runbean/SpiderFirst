# -*- coding: utf-8 -*-
# @Time    : 2018/9/16 13:41
# @Author  : duziteng
# @File    : Itcast.py

import scrapy
from SpiderFirst.items import ItcastItem


class ItcastSpider(scrapy.Spider):

    #爬虫名称
    name = "itcast"
    # 允许的作用域
    allowed_domains = ["itcast.cn"]
    # 爬虫的起始url
    start_urls = ["http://www.itcast.cn/channel/teacher.shtml"]
    # 解析的方法 每个初始url完成下载后被调用

    def parse(self, response):
       for each in response.xpath("//div[@class='li_txt']"):
           item = ItcastItem()
           name = each.xpath("h3/text()").extract()
           leve = each.xpath("h4/text()").extract()
           info = each.xpath("p/text()").extract()
           item["name"] = name[0]
           item["leve"] = leve[0]
           item["info"] = info[0]
           #将获取的数据交给pipelines
           yield item